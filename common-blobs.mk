PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/google/lib,system/lib)
#ifeq ($(TARGET_ARCH),arm64)
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/google/lib64,system/lib64)
#endif
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/google/media,system/media)
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/google/usr,system/usr)

PRODUCT_COPY_FILES += \
    vendor/google/etc/default-permissions/default-permissions.xml:system/etc/default-permissions/default-permissions.xml \
    vendor/google/etc/default-permissions/google-permissions.xml:system/etc/default-permissions/google-permissions.xml \
    vendor/google/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
    vendor/google/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml \
    vendor/google/etc/permissions/com.google.widevine.software.drm.xml:system/etc/permissions/com.google.widevine.software.drm.xml \
    vendor/google/etc/permissions/privapp-permissions-google.xml:system/etc/permissions/privapp-permissions-google.xml \
    vendor/google/etc/sysconfig/google.xml:system/etc/sysconfig/google.xml \
    vendor/google/etc/sysconfig/google_build.xml:system/etc/sysconfig/google_build.xml \
    vendor/google/etc/sysconfig/google-hiddenapi-package-whitelist.xml:system/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
    vendor/google/etc/preferred-apps/google.xml:system/etc/preferred-apps/google.xml

#ifeq ($(IS_PHONE),true)
PRODUCT_COPY_FILES += \
    vendor/google/etc/permissions/com.google.android.dialer.support.xml:system/etc/permissions/com.google.android.dialer.support.xml
#endif
