ifeq ($(TARGET_G_ARCH),)
$(error "GAPPS: TARGET_G_ARCH is undefined")
endif

ifneq ($(TARGET_G_ARCH),arm64)
$(error "GOOGLE: Only arm64 is allowed")
endif

#TARGET_MINIMAL_APPS ?= false

$(call inherit-product, vendor/google/common-blobs.mk)

# Include package overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += vendor/google/overlay
DEVICE_PACKAGE_OVERLAYS += \
    vendor/google/overlay/common/

# framework
PRODUCT_PACKAGES += \
    com.google.android.dialer.support \
    com.google.android.maps \
    com.google.android.media.effects \
    com.google.widevine.software.drm

#ifeq ($(IS_PHONE),true)
#PRODUCT_PACKAGES += \
#    com.google.android.dialer.support
#endif

# app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    Chrome \
    CloudPrint2 \
    Drive \
    FaceLock \
    GoogleCalendarSyncAdapter \
    GoogleContactsSyncAdapter \
    GoogleExtShared \
    GooglePrintRecommendationService \
    GoogleTTS \
    GoogleVrCore \
    LatinIMEGooglePrebuilt \
    Maps \
    MarkupGoogle \
    NexusLauncherIcons \
    Photos \
    PlayGames \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    PrebuiltExchange3Google \
    PrebuiltGmail \
    SoundPickerPrebuilt \
    talkback \
    WallpaperPickerPrebuilt \
    WallpapersUsTwo \
    WebViewStub

#ifeq ($(IS_PHONE),true)
#PRODUCT_PACKAGES += \
#    PrebuiltBugle
#endif

#ifeq ($(TARGET_MINIMAL_APPS),false)
#PRODUCT_PACKAGES += \
#    CalendarGooglePrebuilt \
#    Photos
#endif

# priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    AndroidPlatformServices \
    ConfigUpdater \
    GoogleBackupTransport \
    GoogleContacts \
    GoogleDialer \
    GoogleExtServices \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePackageInstaller \
    GooglePartnerSetup \
    GoogleRestore \
    GoogleServicesFramework \
    MatchmakerPrebuilt \
    Phonesky \
    PixelLauncher \
    PrebuiltGmsCorePi \
    SettingsIntelligenceGooglePrebuilt \
    SetupWizard \
    StorageManagerGoogle \
    TagGoogle \
    TimeZoneDataPrebuilt \
    Turbo \
    Velvet \
    WellbeingPrebuilt

# Fonts
PRODUCT_COPY_FILES += \
    vendor/google/fonts/GoogleSans-Regular.ttf:system/fonts/GoogleSans-Regular.ttf \
    vendor/google/fonts/GoogleSans-Medium.ttf:system/fonts/GoogleSans-Medium.ttf \
    vendor/google/fonts/GoogleSans-MediumItalic.ttf:system/fonts/GoogleSans-MediumItalic.ttf \
    vendor/google/fonts/GoogleSans-Italic.ttf:system/fonts/GoogleSans-Italic.ttf \
    vendor/google/fonts/GoogleSans-Bold.ttf:system/fonts/GoogleSans-Bold.ttf \
    vendor/google/fonts/GoogleSans-BoldItalic.ttf:system/fonts/GoogleSans-BoldItalic.ttf

ADDITIONAL_FONTS_FILE := vendor/google/fonts/google-sans.xml

#ifeq ($(IS_PHONE),true)
#PRODUCT_PACKAGES += \
#    GoogleDialer
#endif

#ifeq ($(TARGET_MINIMAL_APPS),false)
#PRODUCT_PACKAGES += \
#    Velvet
#endif
